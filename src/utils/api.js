import * as conf from '../../bibliotec.conf.json'
import { getToken } from './app.js'
import { progress } from './store.js'
import { NotAuthenticatedException } from './exceptions.js'


const INSTANCE_HOST = conf['instance_host'] || 'http://localhost:8000'

export const APIRequest = (route, data, isForm) => {
    return getToken()
        .then(token=> {
            let method = 'GET'

            if (typeof(isForm) === 'undefined') isForm = false

            else method = 'POST'

            const baseURL = new URL(`${INSTANCE_HOST}/api/v1/`)

            const url = new URL(route, baseURL)

            const request = {
                method: method,
                headers: {
                    'Authorization': `Bearer ${token}`
                },
            }

            if (!isForm) request['headers']['Content-type'] = 'application/json'

            if (typeof('data') !== 'undefined') request['body'] = data

            progress.set(true)

            return fetch(url, request)
        })
        .then(r => {
            progress.set(false)

            if (!r.ok) throw new NotAuthenticatedException()

            return r
        })
        .catch(e => {
            if (window.location.pathname !== '/')
                window.history.pushState('', '', '/')

            throw new NotAuthenticatedException()

            return e
         })
}

export const verifyCredentials = () => {
    const route = 'users/verify_credentials'

    return APIRequest(route)
        .then(r => {
            return r.json()
        })
}
