export function InstanceNotFoundException(message) {
    this.message = message ? message : 'Instance not found'
    this.name = 'InstanceNotFoundException'
}
export function NotAuthenticatedException(message) {
    this.message = message ? message : 'User not authenticated'
    this.name = 'NotAuthenticatedException'
}
