import { goto } from '@roxi/routify'

import * as conf from '../../bibliotec.conf.json'
import { progress } from './store.js'
import { NotAuthenticatedException } from './exceptions.js'


const clientHost = conf['client_host'] || 'http://localhost:5000'

export const registerApp = async(instanceHost) => {
    const appRegistrationURL = `${instanceHost}/api/v1/apps/`

    const body = {
        'name': 'Bibliotec web client',
        'client_type': 'public',
        'authorization_grant_type': 'authorization-code',
        'redirect_uris': `${clientHost}`,
    }

    progress.set(true)

    return fetch(appRegistrationURL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(body)
        }
    )
        .catch(() => { progress.set(false) })
        .then(res => {
            progress.set(false)
            return res.json() })
        .then(json => {
            json['instance_host'] = instanceHost

            localStorage.setItem('auth', JSON.stringify(json))

            progress.set(false)

            window.location.href = (
                `${instanceHost}/o/authorize?` +
                `response_type=code` +
                `&client_id=${json.client_id}` +
                `&redirect_uri=${json.redirect_uris}`)
        })
}

export const getAuthDetails = () => {
    const auth = JSON.parse(localStorage.getItem('auth'))

    return new Promise((resolve, reject) => {
        if (auth === null) {
            reject()

            throw new NotAuthenticatedException()
        }
        else {
            resolve(auth)
        }
    })
}

export const getToken = () => {
    return getAuthDetails()
        .then(auth => {
            const token = auth.access_token
            if (typeof(token) !== 'undefined') return token
            throw new NotAuthenticatedException()
        })
}

export const generateToken = async(code) => {
    return getAuthDetails()
        .then(auth => {
            const appTokenURL = `${auth['instance_host']}/o/token/`

            auth['code'] = code

            const body = {
                'client_id': auth.client_id,
                'client_secret': auth.client_secret,
                'code': code,
                'redirect_uri': auth.redirect_uris,
                'grant_type': 'authorization_code'
            }

            const form = new FormData()

            for (let key in body)
                form.append(key, body[key])

            progress.set(true)

            return fetch(appTokenURL, {
                    method: 'POST',
                    body: form
            })
                .then(res => {
                    progress.set(false)

                    if (!res.ok) throw new Error('Failed to generate token.')

                    return res.json()})
                .then(json => {
                    auth = {
                        ...auth,
                        ...json
                    }

                    localStorage.setItem('auth', JSON.stringify(auth))

                    return json
                })
        })
}

export const logOut = async() => {
    return getAuthDetails()
        .then(auth => {
            const tokenRevokeURL = `${auth['instance_host']}/o/revoke_token/`

            const body = {
                'client_id': auth.client_id,
                'client_secret': auth.client_secret,
                'token': auth.access_token
            }

            const form = new FormData()

            for (let key in body)
                form.append(key, body[key])

	        localStorage.removeItem('auth')

            progress.set(true)

            return fetch(tokenRevokeURL, {
                    headers: { 'Authorization': `Bearer ${auth.access_token}`},
                    method: 'POST',
                    body: form
            })
                .then(() => { progress.set(false) })
        })
}

